package com.weather.dsx.security

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner
import org.scalatest.matchers.ShouldMatchers
import com.weather.dsx.security._
import OtpHelpers._
import com.weather.dsx.security.TotpClock

@RunWith(classOf[JUnitRunner])
class OtpProviderTest extends FlatSpec with ShouldMatchers{

  "prePadTo" should "prepad str to specified len with elem" in {
    999.prePadTo(9, "0") should equal("000000999")
  }

  "hex2bytes" should "convert hex string to bytes" in {
    hex2bytes("abcdef0123456789") should be(Array(-85, -51, -17, 1, 35, 69, 103, -119))
    hex2bytes("0987654321fedcba") should be(Array(9, -121, 101, 67, 33, -2, -36, -70))
  }

  private def generateTestVectors() = {

    val secret = "3132333435363738393031323334353637383930"

    val secret32 = "3132333435363738393031323334353637383930" +
      "313233343536373839303132"

    val secret64 = "3132333435363738393031323334353637383930" +
      "313233343536373839303132333435363738393031323334353" +
      "6373839303132333435363738393031323334"

    val totpClock = TotpClock()
    for {
      testTime <- List(59L, 1111111109L, 1111111111L, 1234567890L, 2000000000L, 20000000000L)
    } yield {
      val msg = totpClock.valueAtTime(testTime).toString
      Array(OtpSha1.otp(secret, msg, 8),
        OtpSha256.otp(secret32, msg, 8),
        OtpSha512.otp(secret64, msg, 8))
    }
  }

  "generateTOTP" should "generate output matching reference vectors" in {
    val expected = List(
      "94287082", "46119246", "90693936",
      "07081804", "68084774", "25091201",
      "14050471", "67062674", "99943326",
      "89005924", "91819424", "93441116",
      "69279037", "90698825", "38618901",
      "65353130", "77737706", "47863826")
    generateTestVectors().flatten should equal(expected)
  }
}
