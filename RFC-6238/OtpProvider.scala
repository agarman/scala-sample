package com.weather.dsx.security

object OtpHelpers {
  class StrHelper(s: Any) {
    val r = s.toString
    def prePadTo(len: Int, elem: Any) = elem.toString * (len - r.length) + r
  }

  implicit def ToStrHelper(s: Any) = new StrHelper(s)

  // produces a positive int from the 1st 4 bytes in a hash
  def hash2Int(hash: Array[Byte], start: Int): Int =
    ((hash(start) & 0x7f) << 24) |
      ((hash(start + 1) & 0xff) << 16) |
      ((hash(start + 2) & 0xff) << 8) |
      (hash(start + 3) & 0xff)

  def hex2bytes(hex: String) = new java.math.BigInteger("10" + hex, 16).toByteArray.drop(1)
  def long2Hex(l: Long) = java.lang.Long.toHexString(l).toUpperCase
  def millis2Seconds(millis: Long) = millis / 1000
  val millisPerMinute = 60 * 1000
}

case class TotpClock(correctionMinutes: Int = 0, startTime: Int = 0, stepTime: Int = 30) {
  import OtpHelpers._

  private val timeCorrectionMinutes = new java.util.concurrent.atomic.AtomicInteger(correctionMinutes)
  def setTimeCorrectionMinutes(minutes: Int) = timeCorrectionMinutes.set(correctionMinutes)
  def getTimeCorrectionMinutes = timeCorrectionMinutes.get()

  def currentMillis = System.currentTimeMillis() + (getTimeCorrectionMinutes * millisPerMinute)

  def valueNow() = valueAtTime(millis2Seconds(currentMillis))

  def valueAtTime(time: Long) = {
    val T = {
      val t = time - startTime
      if (t >= 0) t / stepTime
      else       (t - (stepTime - 1)) / stepTime
    }
    long2Hex(T).prePadTo(16, "0")
  }
}

trait OtpProvider {
  import OtpHelpers._

  val hmac: (Array[Byte], Array[Byte]) => Array[Byte]

  protected def hmac_sha(crypto: String) = {
    val mac = javax.crypto.Mac.getInstance(crypto)

    (key: Array[Byte], message: Array[Byte]) => {
      val secret = new javax.crypto.spec.SecretKeySpec(key, "RAW")
      mac.init(secret)
      mac.doFinal(message)
    }
  }

  // produced with Array.tabulate(9)(i=>"1".padTo(i+1,"0").mkString.toInt)
  private val cd2Int = Array(1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000)

  private def generateOtp(key: String, time: String, codeLength: Int): String = {
    val msg = hex2bytes(time.prePadTo(16, "0"))
    val k = hex2bytes(key)
    generateOtp(k, msg, codeLength)
  }

  private def generateOtp(key: Array[Byte], msg: Array[Byte], codeLength: Int): String = {
    assert(Array(6,7,8).contains(codeLength), "code length 6, 7 & 8 are supported")
    val hash = hmac(key, msg)
    val offset = hash(hash.length - 1) & 0xf
    val binary = hash2Int(hash, offset)
    val otp = binary % cd2Int(codeLength)
    otp.prePadTo(codeLength, "0")
  }

  def otp(secret: String, message: String, codeLength: Int = 6): String =
    generateOtp(secret, message, codeLength)

  def totp(secret: String, codeLength: Int = 6, clock: TotpClock = TotpClock()): String =
    generateOtp(secret, clock.valueNow(), codeLength)
}

object OtpSha1   extends OtpProvider { val hmac = hmac_sha("HmacSHA1")   }
object OtpSha256 extends OtpProvider { val hmac = hmac_sha("HmacSHA256") }
object OtpSha512 extends OtpProvider { val hmac = hmac_sha("HmacSHA512") }

